import { Component } from "@angular/core";
import { RouterExtensions } from "nativescript-angular/router";
import { OnInit } from "@angular/core/src/metadata/lifecycle_hooks";
import * as PayPal from "nativescript-paypal";
import { payPalConfig } from "../../config";
@Component({
  selector: "paypal",
  styleUrls: ["./component/paypal/paypal.component.css"],
  templateUrl: "./component/paypal/paypal.component.html",
})
export class PaypalComponent implements OnInit {
  // Your TypeScript logic goes here
  public email: String;
  public password: String;

  constructor(private routerExtensions: RouterExtensions) {

  }

  ngOnInit(){
      console.log('Paypal Component');
      PayPal.addLogger((msg) => {
        console.log('[nativescript-paypal] ' + msg);
    });
    
    // initialize PayPal environment
    //create config file and set client id and environment
    //if you want to use sand box set environment as 0
    PayPal.init(payPalConfig);    
  }
  

  startPayPalCheckout(){
       // create and setup
        // a new payment
        var payment = PayPal.newPayment()
            .setDescription('Test product')
            .setAmount(10.23)
            .setCurrency('USD');
        // start the checkout    
        payment.start((result) => {
            var logPrefix = '[payment result] ';
            console.log(logPrefix + 'code: ' + result.code);
            
            switch (result.code) {
                case 0:
                    console.log(logPrefix + 'Success: ' + result.key); 
                    break;
                    
                case 1:
                    console.log(logPrefix + 'Operation was cancelled.');
                    break;
                    
                case -1:
                    console.log(logPrefix + 'Checkout failed!');
                    break;
                    
                case -2:
                    console.log(logPrefix + 'Unhandled exception!');
                    break;
                    
                default:
                    console.log(logPrefix + 'UNKNOWN: ' + result.code);
                    break;
            }
        });
    };
  
}

