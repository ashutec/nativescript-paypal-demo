import { NgModule, NO_ERRORS_SCHEMA } from "@angular/core";
import { NativeScriptModule } from "nativescript-angular/nativescript.module";

import { NativeScriptFormsModule } from "nativescript-angular/forms";
import { NativeScriptRouterModule } from "nativescript-angular/router";
import { navigatableComponents, routes } from "./app.routing";
import { AppComponent } from "./app.component";
import { PaypalComponent } from "./component/paypal/paypal.component";


@NgModule({
  declarations: [AppComponent,navigatableComponents],
  bootstrap: [PaypalComponent],
  imports: [NativeScriptModule, 
     NativeScriptRouterModule,
    NativeScriptRouterModule.forRoot(routes)
  ],
  schemas: [NO_ERRORS_SCHEMA],
})
export class AppModule {}
