import { PaypalComponent } from './component/paypal/paypal.component';

export const routes = [
  { path: "paypal", component: PaypalComponent }

];

export const navigatableComponents = [
  PaypalComponent
];
